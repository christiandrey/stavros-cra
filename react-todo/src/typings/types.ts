import { Todo } from "../classes/Todo";

export type AppState = {
	todos: Array<Todo>;
	todoToCreate?: Todo;
};
