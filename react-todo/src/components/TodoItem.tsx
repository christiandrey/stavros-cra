import React from "react";
import { Todo } from "../classes/Todo";

export type TodoItemProps = {
	todo?: Todo;
	index: number;
	preview?: boolean;
	handleClickCompleteTodo?: (id: number) => void;
	handleDeleteTodo?: (id: number) => void;
};

const TodoItem: React.StatelessComponent<TodoItemProps> = ({ todo, preview, index, handleClickCompleteTodo, handleDeleteTodo }) => (
	<div className={`todo-item ${todo.completed && "todo-item--preview"} ${preview && "todo-item--preview"}`} style={{ animationDelay: `${index * 0.1}s` }}>
		<h5 className="todo-item__title">
			{todo.id}. {todo.title}
		</h5>
		{todo.completed && <span className="todo-item__status badge badge-success">Completed</span>}
		{todo.completed || <span className="todo-item__status badge badge-secondary">Not Completed</span>}
		<p className="todo-item__description">{todo.description}</p>
		<div className="btn-group">
			{todo.completed || (
				<button className="btn btn-primary" onClick={() => handleClickCompleteTodo(todo.id)} disabled={preview}>
					Mark as completed
				</button>
			)}
			<button className="btn btn-danger" onClick={() => handleDeleteTodo(todo.id)} disabled={preview}>
				Delete
			</button>
		</div>
	</div>
);

export default TodoItem;
