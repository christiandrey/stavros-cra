import React from "react";

export type TodoInputProps = {
	title: string;
	description: string;
	handleChangeTitle: (e: any) => void;
	handleChangeDescription: (e: any) => void;
	handleSubmit: (e: any) => void;
};

const TodoInput: React.StatelessComponent<TodoInputProps> = ({ title, description, handleChangeTitle, handleChangeDescription, handleSubmit }) => (
	<form className="todo-input" onSubmit={handleSubmit}>
		<div className="form-group">
			<label htmlFor="title">Title</label>
			<input type="text" className="form-control" id="title" placeholder="Enter title" value={title} onChange={handleChangeTitle} />
		</div>
		<div className="form-group">
			<label htmlFor="description">Description</label>
			<textarea className="form-control" id="description" placeholder="Enter description" value={description} onChange={handleChangeDescription} />
		</div>
		<button type="submit" className="btn btn-primary btn-block" disabled={!title || !description || title.length === 0 || description.length === 0}>
			Submit
		</button>
	</form>
);

export default TodoInput;
