export class Todo {
	id: number;
	title: string;
	description: string;
	completed: boolean;

	constructor(dto?: Todo | any) {
		dto = dto || {};

		this.id = dto.id;
		this.title = dto.title;
		this.description = dto.description;
		this.completed = dto.completed || false;
	}
}
