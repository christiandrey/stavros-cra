import "./App.css";
import React from "react";
import TodoItem from "./components/TodoItem";
import TodoInput from "./components/TodoInput";

import { AppState } from "./typings/types";
import { Todo } from "./classes/Todo";

class App extends React.Component<{}, AppState> {
	constructor(props: Readonly<{}>) {
		super(props);
		this.state = {
			todos: []
		};
	}

	todosLocalStorageKey = "todos";

	componentDidMount() {
		// ----------------------------------------------------
		// GET EXISTING TODOS AND SET IN STATE
		// ----------------------------------------------------

		const todosString = localStorage.getItem(this.todosLocalStorageKey);

		if (!!todosString) {
			const todos = JSON.parse(todosString) as Array<Todo>;
			this.setState({ todos: todos.map(o => new Todo(o)) });
		}
	}

	updateTodosInState = (todos: Array<Todo>) => {
		this.setState({ todos }, () => localStorage.setItem(this.todosLocalStorageKey, JSON.stringify(todos)));
	};

	handleCreateTodo = () => {
		const todoToCreate = new Todo();
		todoToCreate.id = Math.max(0, ...this.state.todos.map(o => o.id)) + 1;
		todoToCreate.title = "";
		todoToCreate.description = "";
		this.setState({
			todoToCreate
		});
	};

	handleCancelCreateTodo = () => {
		this.setState({
			todoToCreate: null
		});
	};

	handleChangeTitle = (e: any) => {
		const todoToCreate = Object.assign({}, this.state.todoToCreate);
		todoToCreate.title = e.target.value;

		this.setState({ todoToCreate });
	};

	handleChangeDescription = (e: any) => {
		const todoToCreate = Object.assign({}, this.state.todoToCreate);
		todoToCreate.description = e.target.value;

		this.setState({ todoToCreate });
	};

	handleMarkAsCompleted = (id: number) => {
		const todos = this.state.todos.map(todo => {
			if (todo.id === id) todo.completed = true;
			return todo;
		});
		this.updateTodosInState(todos);
	};

	handleDelete = (id: number) => {
		const todos = [...this.state.todos];
		const index = todos.findIndex(o => o.id === id);

		todos.splice(index, 1);

		this.updateTodosInState(todos);
	};

	handleSubmit = (e: any) => {
		e.preventDefault();
		const todoToCreate = this.state.todoToCreate;
		const { title, description } = todoToCreate;

		if (!title || !description || title.length === 0 || description.length === 0) return;

		const todos = this.state.todos;
		const updatedTodos = [todoToCreate, ...todos];

		this.updateTodosInState(updatedTodos);
		this.setState({ todoToCreate: null });
	};

	render() {
		const todos = this.state.todos;
		const todoToCreate = this.state.todoToCreate;

		return (
			<div className="app">
				<h1 className="app__title">My todo application</h1>
				{!!todoToCreate && (
					<TodoInput
						title={todoToCreate.title}
						description={todoToCreate.description}
						handleChangeTitle={this.handleChangeTitle}
						handleChangeDescription={this.handleChangeDescription}
						handleSubmit={this.handleSubmit}
					/>
				)}
				{!!todoToCreate || (
					<button className="btn btn-success form-group" onClick={this.handleCreateTodo}>
						+ Create Todo
					</button>
				)}
				{!!todoToCreate && (
					<button className="btn btn-danger form-group" onClick={this.handleCancelCreateTodo}>
						x Cancel Todo
					</button>
				)}
				{todoToCreate && <TodoItem todo={todoToCreate} index={0} preview />}
				<div className="todos">
					{todos.map((todo, i) => (
						<TodoItem index={i} todo={todo} key={todo.id} handleClickCompleteTodo={this.handleMarkAsCompleted} handleDeleteTodo={this.handleDelete} />
					))}
					{todos.length === 0 && <div className="empty-state">There are no todo items</div>}
				</div>
			</div>
		);
	}
}

export default App;
