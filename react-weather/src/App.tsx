import "./App.scss";
import React from "react";
import { WeatherApp } from "./components/WeatherApp/WeatherApp";

const App: React.FC = () => {
	return <WeatherApp />;
};

export default App;
