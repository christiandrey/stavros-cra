import React from "react";
import WeatherItem from "../WeatherItem/WeatherItem";
import WeatherFc from "../WeatherFc/WeatherFc";

import { WeatherData } from "../../classes/WeatherData";
import { WeatherInfo } from "../../classes/WeatherInfo";
import { TemperatureUnit } from "../../shared/typings";

interface IWeatherAppState {
	selectedWoeid?: number;
	weatherData: Array<WeatherData>;
	weatherForecasts: Array<WeatherInfo>;
	temperatureUnit: TemperatureUnit;
	isGettingData: boolean;
}

const API = {
	getWeatherInfoByWoeid: "https://www.metaweather.com/api/location/" //$ /woeid
};

const WOEIDS = [44418, 2487956, 1118370, 615702, 1398823];

export class WeatherApp extends React.PureComponent<{}, IWeatherAppState> {
	constructor(props: Readonly<{}>) {
		super(props);

		this.state = {
			isGettingData: true,
			weatherData: new Array<WeatherData>(),
			weatherForecasts: new Array<WeatherInfo>(),
			temperatureUnit: "C"
		};
	}

	getDataFromApi = async () => {
		this.setState({ isGettingData: true });

		const weatherDataCollection = new Array<WeatherData>();

		for (let woeid of WOEIDS) {
			const weatherData = await this.getWeatherData(woeid);
			weatherDataCollection.push(weatherData);
		}

		const selectedData = weatherDataCollection[0];

		this.setState({
			weatherData: weatherDataCollection,
			selectedWoeid: selectedData.woeid,
			weatherForecasts: selectedData.consolidated_weather,
			isGettingData: false
		});
	};

	getWeatherData = async (woeid: number): Promise<WeatherData> => {
		try {
			const response = await fetch(API.getWeatherInfoByWoeid + woeid);
			const data = await response.json();
			return Promise.resolve(new WeatherData(data));
		} catch (error) {
			alert("An error occured.");
			return Promise.reject();
		}
	};

	async componentDidMount() {
		this.getDataFromApi();
	}

	handleClickWeatherItem = (selectedWoeid: number) => {
		const weatherInfo = this.state.weatherData.find(o => o.woeid === selectedWoeid);
		const weatherForecasts = weatherInfo.consolidated_weather;

		this.setState({
			selectedWoeid,
			weatherForecasts
		});
	};

	handleClickChangeUnit = () => {
		const { temperatureUnit } = this.state;

		this.setState({
			temperatureUnit: ["C", "F"].find(o => o !== temperatureUnit) as TemperatureUnit
		});
	};

	render() {
		const { weatherData, weatherForecasts, selectedWoeid, isGettingData, temperatureUnit } = this.state;

		return (
			<div className="weather-app">
				{isGettingData && (
					<div className="refreshing-overlay">
						<img src="http://www.stickpng.com/assets/images/59300da13919fe0ee3614de7.png" className="spinner" alt="" />
					</div>
				)}
				<button className="custom-button" onClick={this.getDataFromApi}>
					{isGettingData ? "Refreshing..." : "Refresh data"}
				</button>
				<div className="weather-items">
					{weatherData.map(o => (
						<WeatherItem
							key={o.woeid}
							title={o.title}
							woeid={o.woeid}
							temperatureUnit={temperatureUnit}
							onClick={this.handleClickWeatherItem}
							selected={o.woeid === selectedWoeid}
							weatherInfo={o.consolidated_weather[0]}
						/>
					))}
				</div>
				<button className="custom-button" onClick={this.handleClickChangeUnit}>
					Change unit
				</button>
				<div className="weather-fc">
					<h2 className="weather-fc__title">Forecasts</h2>
					<div className="weather-fc__container">
						{weatherForecasts.map(o => (
							<WeatherFc weatherInfo={o} key={o.id} temperatureUnit={temperatureUnit} />
						))}
					</div>
				</div>
			</div>
		);
	}
}
