import React from "react";

import { WeatherInfo } from "../../classes/WeatherInfo";
import { TemperatureUnit } from "../../shared/typings";
import { convertTemperatureUnit } from "../../shared/utils";

export interface IWeatherFcProps {
	weatherInfo: WeatherInfo;
	temperatureUnit: TemperatureUnit;
}

const WeatherFc: React.StatelessComponent<IWeatherFcProps> = ({ weatherInfo, temperatureUnit }) => {
	const { the_temp, weather_state_name, applicable_date } = weatherInfo;
	return (
		<div className="weather-fc__item">
			<h4>
				{convertTemperatureUnit(the_temp, temperatureUnit)}°{temperatureUnit}
			</h4>
			<h5>{weather_state_name}</h5>
			<div>{new Date(applicable_date).toDateString()}</div>
		</div>
	);
};

export default WeatherFc;
