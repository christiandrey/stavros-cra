import React from "react";

import { WeatherInfo } from "../../classes/WeatherInfo";
import { TemperatureUnit } from "../../shared/typings";
import { convertTemperatureUnit } from "../../shared/utils";

export interface IWeatherItemProps {
	weatherInfo: WeatherInfo;
	title: string;
	woeid: number;
	selected: boolean;
	temperatureUnit: TemperatureUnit;
	onClick: (woeid: number) => void;
}

const WeatherItem: React.StatelessComponent<IWeatherItemProps> = ({ weatherInfo, temperatureUnit, title, woeid, selected, onClick }) => {
	const { id, weather_state_name, the_temp } = weatherInfo;
	const icon = weatherInfo.getIcon();
	return (
		<div className={selected ? "weather-item weather-item--selected" : "weather-item"} onClick={() => onClick(woeid)}>
			<h2 className="weather-item__title">{title}</h2>
			<div className="weather-item__body">
				<img className="weather-item__icon" src={icon} alt="Icon" />
				<h3 className="weather-item__temp">
					{convertTemperatureUnit(the_temp, temperatureUnit)}°{temperatureUnit}
				</h3>
				<h5 className="weather-item__description">{weather_state_name}</h5>
			</div>
		</div>
	);
};

export default WeatherItem;
