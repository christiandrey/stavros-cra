import { WeatherInfo } from "./WeatherInfo";

export class WeatherData {
	consolidated_weather: Array<WeatherInfo>;
	title: string;
	woeid: number;

	constructor(dto: Partial<WeatherData> = {}) {
		this.consolidated_weather = dto.consolidated_weather ? dto.consolidated_weather.map(o => new WeatherInfo(o)) : new Array<WeatherInfo>();
		this.title = dto.title;
		this.woeid = dto.woeid;
	}
}
