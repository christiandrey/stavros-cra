export class WeatherInfo {
	id: string;
	weather_state_name: string;
	weather_state_abbr: string;
	wind_direction_compass: string;
	created: string;
	applicable_date: string;
	min_temp: number;
	max_temp: number;
	the_temp: number;
	wind_speed: number;
	wind_direction: number;
	air_pressure: number;
	humidity: number;
	visibility: number;
	predictability: number;

	getIcon(): string {
		return `https://www.metaweather.com/static/img/weather/${this.weather_state_abbr}.svg`;
	}

	constructor(dto: Partial<WeatherInfo> = {}) {
		this.id = dto.id;
		this.weather_state_name = dto.weather_state_name;
		this.weather_state_abbr = dto.weather_state_abbr;
		this.wind_direction_compass = dto.wind_direction_compass;
		this.created = dto.created;
		this.applicable_date = dto.applicable_date;
		this.min_temp = dto.min_temp;
		this.max_temp = dto.max_temp;
		this.the_temp = dto.the_temp;
		this.wind_speed = dto.wind_speed;
		this.wind_direction = dto.wind_direction;
		this.air_pressure = dto.air_pressure;
		this.humidity = dto.humidity;
		this.visibility = dto.visibility;
	}
}
