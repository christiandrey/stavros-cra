import { TemperatureUnit } from "./typings";

export const convertTemperatureUnit = (value: number, unit: TemperatureUnit) => {
	if (value === 0) return 0;

	if (unit === "C") return value.toFixed(0);

	return (value * (9 / 5) + 32).toFixed(0);
};
