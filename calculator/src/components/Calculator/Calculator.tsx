import "./calculator.scss";
import React from "react";
import { CalculatorElement } from "../../classes/CalculatorElement";
import { Entry } from "../../classes/Entry";

interface CalculatorState {
	entries: Array<Entry>;
	inputValue: number;
}

class Calculator extends React.PureComponent<{}, CalculatorState> {
	constructor(props: Readonly<{}>) {
		super(props);

		this.state = { inputValue: 0, entries: new Array<Entry>() };
	}

	getCalculatorModifierElements = () => {
		const modifiers = ["AC", "±", "%"];
		return modifiers.map(
			o =>
				new CalculatorElement({
					label: o,
					value: o,
					type: "Others"
				})
		);
	};

	getCalculatorNumberElements = () => {
		const numbers = new Array(10).fill(null).map((o, i) => (9 - i).toString());
		return [...numbers, "."].map(
			o =>
				new CalculatorElement({
					label: o,
					value: o,
					type: "Number"
				})
		);
	};

	getCalculatorOperatorElements = () => {
		const operators = ["÷", "×", "-", "+", "="];
		return operators.map(
			o =>
				new CalculatorElement({
					label: o,
					value: o,
					type: "Operator"
				})
		);
	};

	getLastEntry = () => {
		const { entries } = this.state;
		return entries[entries.length - 1];
	};

	getStringValueFromNumber = (value: number) => {
		return value.toLocaleString(undefined, { maximumFractionDigits: 8 });
	};

	handleClearInput = () => {
		this.setState({ inputValue: 0 });
	};

	handlePressPlusMinus = () => {
		this.setState({
			inputValue: this.state.inputValue * -1
		});
	};

	handlePressPercent = () => {
		this.setState({
			inputValue: this.state.inputValue * 0.01
		});
	};

	handlePressCalculatorButton = (element: CalculatorElement) => {
		console.log("element", element);
	};

	handlePressModifierButton = (element: CalculatorElement) => {
		const { value } = element;

		switch (value) {
			case "AC":
				this.handleClearInput();
				break;
			case "±":
				this.handlePressPlusMinus();
				break;
			case "%":
				this.handlePressPercent();
				break;
		}
	};

	handlePressNumberButton = (element: CalculatorElement) => {
		const { inputValue, entries } = this.state;
		const lastEntry = this.getLastEntry();

		if (!!lastEntry && lastEntry.result === inputValue) {
			this.setState({ inputValue: Number(element.value) });
			return;
		}

		this.setState({
			inputValue: Number(inputValue + element.value)
		});
	};

	handlePressOperatorButton = (element: CalculatorElement) => {
		const { entries, inputValue } = this.state;
		const { value } = element;
		const lastEntry = this.getLastEntry();

		if (inputValue === 0) return;

		if (!!lastEntry && lastEntry.operation === "=") return;

		if (entries.length === 0) {
			const entry = new Entry({
				value: inputValue,
				operation: value,
				result: inputValue
			});

			this.setState({
				entries: entries.concat(entry)
			});

			return;
		}

		let result = inputValue;

		switch (lastEntry.operation) {
			case "÷":
				result = lastEntry.result / inputValue;
				break;
			case "×":
				result = lastEntry.result * inputValue;
				break;
			case "-":
				result = lastEntry.result - inputValue;
				break;
			case "+":
				result = lastEntry.result + inputValue;
				break;
			default:
				result = inputValue;
		}

		const entry = new Entry({
			value: inputValue,
			operation: value,
			result
		});

		let entriesToSetInState = element.value === "=" ? [] : entries.concat(entry);

		this.setState({
			entries: entriesToSetInState,
			inputValue: result
		});
	};

	renderCalculatorElement = (element: CalculatorElement, onPress: () => void) => {
		const { label, value, type } = element;
		return (
			<div className={`calculator__body__button calculator__body__button--${type.toLowerCase()}`} id={`button-${label}`} key={label} onClick={onPress}>
				{value}
			</div>
		);
	};

	render() {
		const { inputValue, entries } = this.state;
		return (
			<div className="calculator">
				<div className="calculator__menu-bar">
					<span className="calculator__menu-bar__control" />
					<span className="calculator__menu-bar__control" />
					<span className="calculator__menu-bar__control" />
				</div>
				<div className="calculator__display">
					<div className="calculator__display__small">
						{entries.map((o, i) => (
							<span key={i}>
								{this.getStringValueFromNumber(o.value)}&nbsp;{o.operation}&nbsp;
							</span>
						))}
					</div>
					<div className="calculator__display__big">{this.getStringValueFromNumber(inputValue)}</div>
				</div>
				<div className="calculator__body">
					<div className="calculator__body__grid-left">
						{this.getCalculatorModifierElements().map(o => this.renderCalculatorElement(o, () => this.handlePressModifierButton(o)))}
						{this.getCalculatorNumberElements().map(o => this.renderCalculatorElement(o, () => this.handlePressNumberButton(o)))}
					</div>
					<div className="calculator__body__grid-right">
						{this.getCalculatorOperatorElements().map(o => this.renderCalculatorElement(o, () => this.handlePressOperatorButton(o)))}
					</div>
				</div>
			</div>
		);
	}
}

export default Calculator;
