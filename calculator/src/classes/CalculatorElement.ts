import { CalculatorElementType } from "../typings/typings";

export class CalculatorElement {
	label: string;
	value: string;
	type: CalculatorElementType;

	constructor(dto: Partial<CalculatorElement> = {}) {
		this.label = dto.label;
		this.value = dto.value;
		this.type = dto.type;
	}
}
