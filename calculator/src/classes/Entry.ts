export class Entry {
	value: number;
	operation: string;
	result: number;

	constructor(dto: Partial<Entry> = {}) {
		this.value = dto.value;
		this.operation = dto.operation;
		this.result = dto.result;
	}
}
