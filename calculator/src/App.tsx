import "./css/main.scss";
import "./App.scss";

import React from "react";
import Calculator from "./components/Calculator/Calculator";

const App: React.FC = () => {
	return (
		<div className="app">
			<Calculator />
		</div>
	);
};

export default App;
